<?php

namespace App\Controller;

use App\Entity\Items;
use App\Form\ItemsType;
use App\Repository\ItemsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ItemsController extends AbstractController
{
    /**
     * @Route("/item/create", methods={"POST"})
     */
    public function create(Request $request, ItemsRepository $itemsRepository, EntityManagerInterface $entityManager)
    {
        $items = new Items();
        $form = $this->createForm(ItemsType::class, $items);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if($form->isSubmitted()){
            $items->setCreationDate(new \DateTime($request->request->get('creationDate')));
            $isValid = $items->isValid();

            if(!$isValid){
                return new Response("L'item n'est pas valide", 403);
            }
            return new Response("L'item a été créé", 200);
        }
        return new Response('erreur 405', 405);
    }
}