<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/register", methods={"POST"})
     */
    public function register(Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {


        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if($form->isSubmitted()){
            $user->setBirthDate(new \DateTime($request->request->get('birthDate')));
            $isValid = $user->isValid();

            if(!$isValid){
                return new Response('l\'utilisateur n\'est pas valide', 403);
            }
//            $entityManager->persist($user);
//            $entityManager->flush();

            return new Response("L'utilisateur a créé son compte", 200);
        }

        return new Response('erreur 405', 405);

    }
}