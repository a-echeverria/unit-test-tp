<?php

namespace App\Controller;


use App\Entity\Items;
use App\Entity\Todolists;
use App\Form\TodolistType;
use App\Repository\TodolistsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TodolistController extends AbstractController
{
    /**
     * @Route("/todolist/create", methods={"POST"})
     */
    public function create(Request $request, TodolistsRepository $itemsRepository, EntityManagerInterface $entityManager)
    {
        $todolist = new Todolists();
        $form = $this->createForm(TodolistType::class, $todolist);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if($form->isSubmitted()){
            $isValid = $todolist->isValid();

            if(!$isValid){
                return new Response("La todolist n'est pas valide", 403);
            }
            return new Response("La todolist a été créé", 200);
        }
        return new Response('erreur 405', 405);
    }


    /**
     * @Route("/todolist/add", methods={"POST"})
     */
    public function addItem(Request $request)
    {
        $receiveItem = $request->request->get("item");
        $receiveTodolist = $request->request->get("todolist");

        $todolist = new Todolists();
        $item = new Items();
        $item->setName($receiveItem["name"]);
        $item->setContent($receiveItem["content"]);
        $item->setCreationDate(new \DateTime($receiveItem["creationDate"]));

        $todolist->setName($receiveTodolist["name"]);
        $todolist->setDescription($receiveTodolist["description"]);
        $todolist->setIdUser($receiveTodolist["idUser"]);

        $isValidTodolist = $todolist->isValid();
        $isValidItem = $item->isValid();

        if(!$isValidTodolist  || !$isValidItem)
        {
            return new Response("L'item n'a pas pu etre ajouté", 400);
        }
        $todolist->addItem($item);
        return new Response("L'item a bien été ajouté'", 200);
    }
}