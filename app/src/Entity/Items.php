<?php

namespace App\Entity;

use App\Repository\ItemsRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass=ItemsRepository::class)
 */
class Items
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=Todolists::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $idTodoLists;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIdTodoLists(): ?Todolists
    {
        return $this->idTodoLists;
    }

    public function setIdTodoLists(?Todolists $idTodoLists): self
    {
        $this->idTodoLists = $idTodoLists;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function isValid() {
        return !(strlen($this->getName()) >= 100) &&
            !(strlen($this->getName()) <= 5) &&
            !empty($this->getContent()) &&
            !(strlen($this->getContent()) >= 1000) &&
            !empty($this->getName());
    }
}
