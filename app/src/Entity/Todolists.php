<?php

namespace App\Entity;

use App\Repository\ItemsRepository;
use App\Repository\TodolistsRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TodolistsRepository::class)
 */
class Todolists
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="todolists")
     * @ORM\JoinColumn(nullable=true)
     */
    private $idUser;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Items::class, mappedBy="idTodoLists", orphanRemoval=true)
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Items[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Items $item): self
    {
        if (!($item->isValid()))
        {
            throw new \Exception("L'item n'est pas valide.");
        }

        if (count($this->items) >= 10)
        {
            throw new \Exception("Votre liste contient déjà 10 items.");
        }

        if (count($this->items) === 8)
        {
            // Envoie de mail
        }

        foreach ($this->items as $i)
        {
            if ($item->getName() === $i->getName())
            {
                print_r($item->getName());
                print_r($i->getName());
                throw new \Exception("Le nom est déjà utilisé.");
            }

            if ($i->getCreationDate()->gt(($item->getCreationDate())->subMinutes(30)))
            {
                throw new \Exception("Un item à été crée récemment.");
            }
        }

        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setIdTodoLists($this);
        }

        return $this;
    }

    public function removeItem(Items $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getIdTodoLists() === $this) {
                $item->setIdTodoLists(null);
            }
        }

        return $this;
    }

    public function canAddItem(Items $item) {
        if (empty($item->getContent()) || $item->isValid()) {
            return false;
        }
        if ($this->countItems() >= 10) {
            return false;
        }
        if ($this->getLastItem() !== null && Carbon::now()->subMinutes(30)->IsAfter($this->getLastItem()->getCreationDate())) {
            return false;
        }
        return true;
    }

    protected function getLastItem() {
        $repo = new ItemsRepository;
        $items = $repo->findBy(['id' => $this->getId()]);
        return end($items);
    }

    protected function countItems() {
        return count($this->getItems());
    }

    public function isValid()
    {
        return !empty($this->getName()) &&
            strlen($this->getName()) > 5 &&
            strlen($this->getName()) < 100 &&
            !empty($this->getDescription()) &&
            strlen($this->getDescription()) > 10 &&
            strlen($this->getDescription()) < 200 &&
            gettype($this->getIdUser()) === "integer";
    }
}
