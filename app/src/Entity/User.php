<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=320)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @ORM\OneToOne(targetEntity=Todolists::class, mappedBy="idUser")
     */
    private $todolists;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getTodolists(): ?Todolists
    {
        return $this->todolists;
    }

    public function setTodolists(Todolists $todolists): self
    {
        $this->todolists = $todolists;

        // set the owning side of the relation if necessary
        if ($todolists->getIdUser() !== $this) {
            $todolists->setIdUser($this);
        }

        return $this;
    }

    public function isValid() {
        return !empty($this->getFirstName()) &&
            !empty($this->getLastName()) &&
            !empty($this->getEmail()) &&
            filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL) &&
            !empty($this->getPassword()) &&
            strlen($this->getPassword()) <= 40 &&
            strlen($this->getPassword()) >= 8 &&
            !empty($this->getBirthDate()) &&
            Carbon::now()->subYears(13)->isAfter($this->getBirthDate());
    }
}
