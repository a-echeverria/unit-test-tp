<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201227151826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE todolists_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE items (id INT NOT NULL, id_todo_lists_id INT NOT NULL, name VARCHAR(100) NOT NULL, content VARCHAR(1000) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E11EE94D4D9D09E ON items (id_todo_lists_id)');
        $this->addSql('CREATE TABLE todolists (id INT NOT NULL, id_user_id INT NOT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(1000) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AD289A179F37AE5 ON todolists (id_user_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(100) NOT NULL, email VARCHAR(320) NOT NULL, password VARCHAR(255) NOT NULL, birth_date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94D4D9D09E FOREIGN KEY (id_todo_lists_id) REFERENCES todolists (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE todolists ADD CONSTRAINT FK_AD289A179F37AE5 FOREIGN KEY (id_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94D4D9D09E');
        $this->addSql('ALTER TABLE todolists DROP CONSTRAINT FK_AD289A179F37AE5');
        $this->addSql('DROP SEQUENCE items_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE todolists_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP TABLE items');
        $this->addSql('DROP TABLE todolists');
        $this->addSql('DROP TABLE "user"');
    }
}
