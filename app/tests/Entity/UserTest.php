<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    public function init()
    {
        $this->user = new User();
        $this->user->setFirstName('Andrea');
        $this->user->setLastName('ECHEVERRIA');
        $this->user->setEmail('echeverriaandrea20@gmail.com');
        $this->user->setBirthDate(Carbon::createFromDate('1999', '09', '15'));
        $this->user->setPassword('1234567890Azertyuiop');
    }

    public function testValidUser()
    {
        $this->init();
        $this->assertEquals(true, $this->user->isValid());
    }

    public function testWrongFirstName() {
        $this->init();
        $this->user->setFirstName('');
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testWrongLastName() {
        $this->init();
        $this->user->setLastName('');
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testWrongEmail() {
        $this->init();
        $this->user->setEmail('');
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testNotValidFormatEmail() {
        $this->init();
        $this->user->setEmail('echeverriaandreagmail.com');
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testWrongBirthDate() {
        $this->init();
        $this->user->setBirthDate(Carbon::now());
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testPasswordTooShort() {
        $this->init();
        $this->user->setPassword('1234');
        $this->assertEquals(false, $this->user->isValid());
    }

    public function testPasswordTooLong() {
        $this->init();
        $this->user->setPassword(str_repeat('1234567890', 5));
        $this->assertEquals(false, $this->user->isValid());
    }
}
