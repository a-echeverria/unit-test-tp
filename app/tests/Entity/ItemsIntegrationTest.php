<?php
namespace App\Tests\Entity;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use function GuzzleHttp\Psr7\str;


class ItemsIntegrationTest extends TestCase
{

    protected static $url = "192.168.1.10:8082/";

    public function init()
    {
        $items = [
            "name" => 'Manger',
            "content" => 'manger 5 fruits et légumes par jours',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);
    }

    public function testValidItems()
    {
        $items = [
            "name" => 'Manger',
            "content" => 'manger 5 fruits et légumes par jours',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);

        self::assertEquals(200, $response->getStatusCode());
    }

    public function testWrongName()
    {
        $items = [
            "name" => '',
            "content" => 'manger 5 fruits et légumes par jours',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);


        self::assertEquals(403, $response->getStatusCode());
    }

    public function testNameTooLong()
    {
        $items = [
            "name" => str_repeat('Manger', 100),
            "content" => 'manger 5 fruits et légumes par jours',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testNameTooShort()
    {
        $items = [
            "name" => 'M',
            "content" => 'manger 5 fruits et légumes par jours',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testContentTooLong()
    {
        $items = [
            "name" => 'Manger',
            "content" => str_repeat('123456789100', 100),
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url . 'item/create', ["form_params" => $items, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }
}