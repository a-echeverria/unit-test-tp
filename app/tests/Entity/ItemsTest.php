<?php

namespace App\Tests\Entity;

use App\Entity\Items;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class ItemsTest extends TestCase
{
    private $item;

    public function init()
    {
        $this->item = new Items();
        $this->item->setName('Manger');
        $this->item->setContent('manger 5 fruits et légumes par jours');
        $this->item->setCreationDate(Carbon::now()->subHour());

    }

    public function testValidItem()
    {
        $this->init();
        $this->assertEquals(true, $this->item->isValid());
    }

    public function testWrongName() {
        $this->init();
        $this->item->setName('');
        $this->assertEquals(false, $this->item->isValid());
    }

    public function testNameTooLong() {
        $this->init();
        $this->item->setName(str_repeat('a', 110));
        $this->assertEquals(false, $this->item->isValid());
    }

    public function testNameTooShort() {
        $this->init();
        $this->item->setName('aze');
        $this->assertEquals(false, $this->item->isValid());
    }

    public function testContentTooLong() {
        $this->init();
        $this->item->setContent(str_repeat('a', 1100));
        $this->assertEquals(false, $this->item->isValid());
    }
}