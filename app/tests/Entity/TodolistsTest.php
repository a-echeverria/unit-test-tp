<?php

namespace App\Tests\Entity;

use App\Entity\Items;
use App\Entity\Todolists;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class TodolistsTest extends TestCase
{
    private $todolist;
    private $item;
//    private $mockTodolist;

    public function init() {
        $this->todolist = new Todolists();
        $this->todolist->setName('A faire');
        $this->todolist->setDescription('Une petite liste de ce que je dois faire !');
        $this->todolist->setIdUser(1);

        $this->item = new Items();
        $this->item->setName('Manger');
        $this->item->setContent('manger 5 fruits et légumes par jours');
        $this->item->setCreationDate(Carbon::now()->subHour());

        $this->todolist->addItem($this->item);
        
//        $this->mockTodolist = $this->getMockBuilder(Todolists::class)
//            ->onlyMethods(['getLastItem', 'countItems'])
//            ->getMock();
    }

    public function testValidList() {
        $this->init();
        $this->assertEquals(true, $this->todolist->isValid());
    }

    public function testNameTooShort()
    {
        $this->init();
        $this->todolist->setName('TODO');
        $this->assertEquals(false, $this->todolist->isValid());
    }

    public function testNameTooLong()
    {
        $this->init();
        $this->todolist->setName(str_repeat('TODO', 100));
        $this->assertEquals(false, $this->todolist->isValid());
    }

    public function testDescriptionTooShort()
    {
        $this->init();
        $this->todolist->setDescription('Desc');
        $this->assertEquals(false, $this->todolist->isValid());
    }

    public function testDescriptionTooLong()
    {
        $this->init();
        $this->todolist->setDescription(str_repeat('Desc', 100));
        $this->assertEquals(false, $this->todolist->isValid());
    }

    public function testAddItemWithSameName()
    {
        $this->init();
        $this->expectException('Exception');
        $this->todolist->addItem($this->item);
    }

    public function testAddItem()
    {
        $this->init();

        $item = new Items();
        $item->setName('Boiree');
        $item->setContent('Boire au moins 2 litres d\'eau par jours');
        $item->setCreationDate(Carbon::now());

        $this->assertEquals($this->todolist, $this->todolist->addItem($item));
    }

    public function testAddItemMaxAmountOfItem()
    {
        $this->init();
        $i = 1;
        for ($j = 0 ; $j < 9; $j++)
        {
            $item = new Items();
            $item->setName(str_repeat('aaaaaa', $i));
            $item->setContent(str_repeat('aa', $i));
            $item->setCreationDate(Carbon::now()->addHours(1 + $i));
            $this->todolist->addItem($item);
            $i++;
        }
        $this->expectException('Exception');
        $this->todolist->addItem($this->item);
    }

    public function testAddItemLessThanThirtyMinutesLater()
    {
        $this->init();

        $item = new Items();
        $item->setName('Boiree');
        $item->setContent('Boire au moins 2 litres d\'eau par jours');
        $item->setCreationDate(Carbon::now()->subMinutes(45));

        $this->expectException('Exception');
        $this->todolist->addItem($item);
    }

    // Ne marche pas
//    public function testCanAddItem() {
//        $lastItem = new Items();
//        $lastItem->setName('Boir');
//        $lastItem->setContent('Boir au moin 2 littres d\'eau par jours');
//        $lastItem->setCreationDate(Carbon::now()->subHours(2));
//
//        $this->init();
//        $this->mockTodolist->expects($this->once())->method('countItems')->willReturn(4);
//        $this->mockTodolist->expects($this->any())->method('getLastItem')->willReturn($lastItem);
//
//        $this->assertEquals(true, $this->todolist->canAddItem($this->item));
//    }
}
