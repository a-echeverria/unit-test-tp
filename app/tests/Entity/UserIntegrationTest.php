<?php
namespace App\Tests\Entity;

use Carbon\Carbon;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;


class UserIntegrationTest extends TestCase
{

    protected static $url = "192.168.1.10:8082/";
    public function init()
    {
        $user = [
            'firstname' => "Andrea",
            'lastname' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthdate' => Carbon::createFromDate('1999', '09', '15'),
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);
    }

    public function testValidUser()
    {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(200, $response->getStatusCode());
    }

    public function testWrongFirstName() {
        $user = [
            'firstName' => "",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);



        self::assertEquals(403, $response->getStatusCode());
    }


    public function testWrongLastName() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testWrongEmail() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testNotValidFormatEmail() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandreal.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testWrongBirthDate() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2012-08-15T15:52:01",
            'password' => "1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testPasswordTooShort() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "123"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testPasswordTooLong() {
        $user = [
            'firstName' => "Andrea",
            'lastName' => "ECHEVERRIA",
            'email' => "echeverriaandrea20@gmail.com",
            'birthDate' => "2005-08-15T15:52:01",
            'password' => "1234567890Azertyuiop1234567890Azertyuiop1234567890Azertyuiop1234567890Azertyuiop1234567890Azertyuiop"
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'register', ["form_params" => $user, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }
}