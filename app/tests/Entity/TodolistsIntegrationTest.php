<?php

namespace App\Tests\Entity;


use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class TodolistsIntegrationTest extends TestCase
{
    protected static $url = "192.168.1.10:8082/";

    public function init()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A faire",
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

    }

    function testValidList() {

        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A faire",
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(200, $response->getStatusCode());
    }

    public function testNameTooShort()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A",
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testNameTooLong()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => str_repeat("A", 1000),
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testDescriptionTooShort()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A",
            "description" => "Un",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testDescriptionTooLong()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A",
            "description" => str_repeat("Une petite liste de ce que je dois faire !", 50),
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testAddItemWithSameName()
    {
        $item = [
            "name" => "Manger",
            "content" => 'Une pomme',
            "creationDate" => "2021-02-11T15:52:01"
        ];

        $todolist = [
            "name" => "A",
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
            "item" => $item,
        ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/create', ["form_params" => $todolist, "http_errors" => false]);

        self::assertEquals(403, $response->getStatusCode());
    }

    public function testAddItem()
    {
        $todolist = [
            "name" => "Ma liste",
            "description" => "Une petite liste de ce que je dois faire !",
            "idUser" => 1,
        ];

        $item2 = [
            "name" => "Boire !!!",
            "content" => "Ouai de l'eau",
            "creationDate" => "2021-02-11T15:52:01",
            ];

        $client = new Client();
        $response = $client->request("POST", self::$url.'todolist/add', ["form_params" => ["todolist" => $todolist, "item" => $item2], "http_errors" => false]);

        self::assertEquals(200, $response->getStatusCode());
    }
}
